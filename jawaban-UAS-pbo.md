# 1 Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 
|User|Bisa|Nilai|
|----|----|-----|
User|Registrasi✅ |100|
User|Login✅ |100|
User|Pesan Tiket Kereta Api✅ |100|
User|Membeli saldo E-wallet✅ |100|
User|Membayar tiket✅ |100|
User|Pesan Tiket Pesawat|80
User|Pesan Sewa Mobil|80
User|Pesan Tiket Hotel|80
User|Memilih Fasilitas Bagasi Pesawat Internasional|80
User|Check in Pesawat Internasional|79
User|Check in Pesawat Domestik|78
User|Memilih Fasilitas Bagasi Pesawat Domestik|77
User|Mengetahui Syarat dan ketentuan Penerbangan Domestik|76
User|Memilih List Maskapai Domestik yang tersedia|75
User|Online Check in tiket.com|74
User|Mendapat Asuransi Pesawat|73
User|Klaim Asuransi Pesawat Domestik|72
User|Melihat info umum tiket hotel|71
User|Melihat informasi waktu check in dan check out hotel|70
User|Konsultasi kendala pemesanan tiket hotel|70
User|Memilih fasilitas tambahan pemesanan hotel|70
User|Bayar di hotel|70
User|Konsultasi e Tiket tidak terbit|70
User|Mendapat asuransi hotel|70
User|Menulis ulasan hotel|70
User|Chat dengan hotel|70
User|Menghubungi dan menemukan lokasi hotel|70
User|Mengetahui informasi umum ticket To Do|70
User|Pemesanan To Do|60
User|Cara mengakses tiket Live|60
User|Sewa video di tiket.com|60
User|Pesan tiket To Do di tiket.com|60
User|Konsultasi perubahan event mengalami perubahan|60
User|Mengetahui informasi tiket JR Pass|60
User|Penukaran To Do|60
User|Mengetahui info kontak dan lokasi To Do|60
User|Penukaran tiket To Do di tiket.com|60
User|Mengetahui ketentuan pemesanan tiket kereta api di tiket.com|50
User|Mendapat asuransi perjalanan kereta api di tiket.com|50
User|Membayar biaya tambahan sewa mobil|50
User|Mengetahui ketentuan umum sewa mobil|50
User|Pesan jemputan bandara|50
User|Mengetahui lokasi jemputan bandara|50
User|Mengetahui ketentuan umum pemesanan jemputan bandara|50
User|Pemesanan bus dan travel|40
User|Mengetahui informasi pembayaran produk tiket.com|40
User|Mengetahui mata uang yang tersedia untuk pembayaran tiket.com|30
User|Membayar dengan ATM|30
User|Membayar dengan kartu debit|30
User|Membayar dengan Transfer bank|30
User|Membayar dengan Virtual Account|30
User|Membayar Instan|30
User|Membayar dengan gerai retail|30
User|Membayar dengan cicilan tanpa kartu kredit|30
User|Mengetahui informasi kartu kredit|30
User|Menyimpan dan menghapus data kartu kredit|30
User|Pengajuan pay later by indodana|30
User|Pengajuan kartu kredit BCA by tiket.com|30
User|Mengaktifkan akun Blipay di tiket.com jika sudah memiliki akun Blipay|30
User|Mengaktifkan akun Blipay di tiket.com jika belum memiliki akun Blipay|30
User|Refund dan Withdraw Blipay|30
User|Menonaktifkan Blipay di tiket.com|30
User|Membatalkan Tiket Pesawat Internasional|20
User|Cek status proses refund Pesawat Internasional|20
User|Mengubah jadwal / reschedule tiket pesawat internasional yang sudah dipesan|20
User|Mendapatkan informasi perubahan jadwal dari maskapai internasional|20
User|Pembatalan tiket pesawat domestik|20
User|Cek status proses refund pesawat domestik|20
User|Mengubah jadwal / reschedule tiket pesawat domestik yang sudah dipesan|20
User|Mendapatkan informasi perubahan jadwal dari maskapai domestik|20
User|Pembatalan pemesanan tiket hotel non - refundable|20
User|Mengajukan reschedule tiket hotel|20
User|Pembatalan tiket hotel|20
User|Perubahan jadwal pemesanan tiket hotel non - refundable|20
User|Cek status refund tiket hotel|20
User|Perubahan dan pengembalian dana atau refund tiket To Do|20
User|Ubah pemesanan tiket kereta|20
User|Pembatalan / refund pemesanan tiket kereta api|20
User|Perubahan jadwal sewa mobil|20
User|Pembatalan sewa mobil|20
User|Refund pemesanan jemputan bandara|20
User|Mengubah pemesanan jemputan bandara|20
User|Refund dan schedule bus dan travel|20
User|Pendaftaran akun tiket.com|10
User|Perubahan informasi tiket.com|10
User|Penghapusan akun|10
User|Menjaga keamanan akun|10
User|Menggunakan promo dan gift voucher|10
User|Menggunakan tiket.com referral|10
User|Mendapatkan tiket CLEAN|10
User|Mengajukan klaim Jaminan Harga Termurah|10
User|Mendapatkan layanan tiket Plus|10
User|Mengajukan kerja sama dengan tiket.com|10
User|Mendapatkan Blibli tiket rewards|10
User|Mendapatkan Blibli tiket points|10
User|Koreksi data penumpang pesawat internasional|10
User|Koreksi data penumpang pesawat domestik|10
User|Ubah nama pemesan hotel|10
User|Ganti permintaan khusus pemesanan hotel|10
User|Perubahan data pemesanan sewa mobil|10
Admin|Menambah data penumpang✅ |100
Admin|Menampilkan data penumpang✅ |100
Admin|Mengubah data penumpang✅ |100
Admin|Menghapus data penumpang✅ |100
Manajemen|Memeriksa pembayaran yang sudah masuk|10
Manajemen|Mengumpulkan data pelanggan|10
Manajemen|Meningkatkan efisiensi operasional|10
Manajemen|Meningkatkan pengalaman pelanggan|10
Manajemen|Meningkatkan keuangan yang lebih efektif|10
Manajemen|Mempromosikan dan memasarkan|10
Direksi|Menyediakan kanal pembayaran tambahan|10
Direksi|Meningkatkan efisiensi dan pengendalian|10
Diresi|Mengambil keputusan berdasarkan data|10
Direksi|Meningkatkan keamanan|10
Direksi|Mengembangkan inovasi|10

# 2 Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
![ClassDiagram](./classDiagram.png)

Penjelasan :
- 'KeretaApiBuilder' hanya digunakan untuk membangun objek 'KeretaApi', tetapi tidak memilikinya sebagai atribut atau bagian dari komposisi, maka hubungan tersebut dapat direpresentasikan dengan hubungan agregasi.
- Dalam diagram diatas, menunjukkan bahwa 'RegulerUser' memiliki ketergantungan pada objek 'KeretaApi'. Hubungan kelas tersebut menujukkan hubungan asosiasi.
- Dalam class diagram, hubungan antara User sebagai kelas abstrak dengan RegularUser, GuestUser, dan KeretaApi sebagai kelas turunan dapat direpresentasikan dengan hubungan pewarisan atau inheritance. Hubungan ini menunjukkan bahwa RegularUser, GuestUser, dan KeretaApi adalah kelas yang mewarisi sifat atau perilaku dari kelas abstrak User.
- Dalam class diagram, hubungan antara TiketWallet dan 'kereta api' setelah pembayaran berhasil dapat direpresentasikan dengan sebuah hubungan ketergantungan (dependency). Hubungan ini menunjukkan bahwa TiketWallet bergantung pada fungsi pencetakan tiket kereta api setelah pembayaran berhasil dilakukan.

# 3 Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

![ClassDiagram](./User.png)
![ClassDiagram](./RegulerUser.png)
![ClassDiagram](./GuestUser.png)
![ClassDiagram](./KeretaApi.png)
![ClassDiagram](./KeretaApiBuilder.png)

Penjelasan : 
Single Responsibility Principle (SRP):
- Kelas User: Bertanggung jawab untuk menyimpan informasi pengguna dan memiliki metode abstrak displayInfo() yang berfokus pada tugas menampilkan informasi pengguna.
- Kelas RegulerUser: Mewarisi kelas User dan tidak menambahkan tanggung jawab baru, hanya menyediakan implementasi konkrit untuk metode displayInfo() sesuai dengan peran pengguna reguler.
- Kelas GuestUser: Mewarisi kelas User dan tidak menambahkan tanggung jawab baru, hanya menyediakan implementasi konkrit untuk metode displayInfo() sesuai dengan peran pengguna tamu.
- Kelas KeretaApi: Mewarisi kelas User dan bertanggung jawab untuk menyimpan informasi tiket kereta api serta memiliki metode displayInfo() yang menampilkan informasi tiket kereta api.
- Kelas KeretaApiBuilder: Bertanggung jawab untuk membangun objek KeretaApi dengan menggunakan metode berantai. Ini adalah tanggung jawab tunggal kelas ini.

Open/Close Principle (OCP):
- Kelas User: Kelas ini terbuka untuk diwarisi dan dapat diperluas dengan kelas-kelas turunannya seperti RegulerUser, GuestUser, dan KeretaApi.
- Kelas KeretaApiBuilder: Kelas ini terbuka untuk diperluas dengan menambahkan metode baru jika diperlukan untuk membangun objek KeretaApi dengan fitur tambahan.

Liskov Subtitution Principle (LSP):
- Kelas RegulerUser dan GuestUser dapat digunakan secara interchangeably dengan kelas User tanpa mempengaruhi fungsionalitas umum yang diharapkan dari objek User.
- Kelas KeretaApi yang mewarisi kelas User juga mematuhi prinsip LSP, sehingga objek KeretaApi dapat digunakan di mana saja di mana objek User diperlukan.

Interface Segregation Principle (ISP):
- Tidak ada

Dependency Inversion Principle (DIP):
- Kelas KeretaApi menerima konstruktor dari kelas KeretaApiBuilder. Ini mematuhi DIP dengan bergantung pada abstraksi User sebagai parameter konstruktor, bukan pada implementasi kelas spesifik seperti RegulerUser atau GuestUser. Ini memungkinkan fleksibilitas dalam penggunaan berbagai jenis pengguna saat membangun objek KeretaApi.

# 4 Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
- Desain Pattern tipe Builder

![ClassDiagram](./desainPattern1.png)

Penjelasan :
- KeretaApi(KeretaApiBuilder builder): Konstruktor ini menerima objek dari kelas KeretaApiBuilder sebagai argumen. Konstruktor ini digunakan untuk menginisialisasi objek KeretaApi dengan nilai-nilai yang diberikan oleh objek builder.

![ClassDiagram](./desainPattern3.png)

Penjelasan :
- KeretaApiBuilder(String username, String password, String nik, String ponsel): Konstruktor ini digunakan untuk menginisialisasi objek KeretaApiBuilder dengan username, password, NIK, dan ponsel yang diberikan.

![ClassDiagram](./desainPattern2.png)

- Membuat objek builder dari kelas KeretaApiBuilder dengan menggunakan konstruktor yang menerima username, password, NIK, dan nomor ponsel sebagai argumen. Ini akan menginisialisasi objek builder dengan nilai-nilai yang diberikan.
- setDari(dari): Mengatur nilai atribut dari pada objek builder dengan nilai yang diberikan oleh variabel dari.
- Memanggil metode build() pada objek builder untuk membangun objek KeretaApi. Metode ini akan mengembalikan objek KeretaApi yang baru dibangun dan disimpan dalam variabel user.

# 5 Mampu menunjukkan dan menjelaskan konektivitas ke database
Konektivitas Registrasi User ke database

![ClassDiagram](./regDB.png)

Penjelasan : 
- String insertQuery = "INSERT INTO user (username, password, nik, nohp) VALUES (?, ?, ?, ?)"; - Mendefinisikan pernyataan SQL untuk menyisipkan (insert) data pengguna ke dalam tabel user. Parameter digunakan sebagai tanda tanya (?) yang akan diisi dengan nilai sebenarnya.
- try (PreparedStatement pst = connection.prepareStatement(insertQuery)) { - Membuat objek PreparedStatement menggunakan koneksi connection untuk mempersiapkan pernyataan SQL yang akan dieksekusi. Objek PreparedStatement memungkinkan penggunaan parameter dalam pernyataan SQL dan melindungi dari serangan injeksi SQL.
- pst.setString(1, username); - Mengatur nilai parameter pertama (?) dalam pernyataan SQL dengan nilai username. 

Konektivitas Login User ke database

![ClassDiagram](./logDB.png)

Penjelasan : 
- String loginQuery = "SELECT * FROM user WHERE username = ? AND password = ?"; - Mendefinisikan pernyataan SQL untuk melakukan pencarian data pengguna berdasarkan username dan password yang diberikan.
- pst.setString(1, loginUsername); - Mengatur nilai parameter pertama (?) dalam pernyataan SQL dengan nilai loginUsername (username yang digunakan untuk login).
- ResultSet result = pst.executeQuery(); - Mengeksekusi perintah SQL untuk melakukan pencarian data pengguna berdasarkan username dan password. Perintah ini akan mengembalikan objek ResultSet yang berisi hasil pencarian.
- if (result.next()) { - Memeriksa apakah objek ResultSet memiliki baris data. Jika result.next() mengembalikan true, artinya ada baris data yang cocok dengan username dan password yang diberikan.

Konektivitas Pencetakan Tiket dari database

![ClassDiagram](./cetakDB.png)

Penjelasan : 
- String selectQuery = "SELECT username, nik, nohp FROM user WHERE username = ?"; - Mendefinisikan pernyataan SQL untuk mengambil data pengguna dari tabel user berdasarkan username yang diberikan. Pernyataan ini akan mengambil kolom username, nik, dan nohp dari baris pengguna yang sesuai.
- get.setString(1, loginUsername); - Mengatur nilai parameter pertama (?) dalam pernyataan SQL dengan nilai loginUsername (username yang digunakan untuk mencari data pengguna).
- String usernameDB = resultset.getString("username"); - Mengambil nilai kolom "username" dari baris hasil pencarian dan menyimpannya ke dalam variabel usernameDB.

# 6 Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
CREATE

![ClassDiagram](./wsCreate.png)

Penjelasan : 
- @app.post("/register") - Ini adalah decorator yang menentukan metode HTTP yang digunakan (POST) dan jalur endpoint (/register) untuk mencocokkan permintaan HTTP yang masuk.
- def register(user: UserRegistration): - Ini adalah definisi fungsi register yang mengambil objek user sebagai parameter. Objek user diketahui mengikuti struktur data UserRegistration.
- cursor = db.cursor() - Membuat objek cursor yang akan digunakan untuk berinteraksi dengan database.
- cursor.execute("SELECT id FROM user WHERE username = %s", (user.username,)) - Eksekusi pernyataan SQL untuk memeriksa apakah ada pengguna dengan username yang sama dalam tabel user.
- cursor.execute("INSERT INTO user (username, password, nik, nohp) VALUES (%s, %s, %s, %s)", (user.username, user.password, user.nik, user.phone_number)) - Eksekusi pernyataan SQL untuk menyimpan data pengguna baru ke dalam tabel user. Nilai-nilai kolom diambil dari atribut-atribut objek user.
- db.commit() - Melakukan commit transaksi ke database untuk menyimpan perubahan.

READ

![ClassDiagram](./wsRead.png)

penjelasan:
- def get_user(id): - Fungsi get_user digunakan untuk mengambil data pengguna berdasarkan ID yang diberikan.
- query = "SELECT * FROM user WHERE id=%s" - Mendefinisikan pernyataan SQL untuk mengambil data pengguna dari tabel user berdasarkan ID yang diberikan.
- cursor.execute(query, (id,)) - Mengeksekusi perintah SQL dengan menggabungkan pernyataan dan nilai ID pengguna yang ingin dibaca.
- @app.get("/user/{id}") - Ini adalah decorator yang menentukan metode HTTP yang digunakan (GET) dan jalur endpoint (/user/{id}) untuk mencocokkan permintaan HTTP yang masuk. {id} adalah parameter jalur yang akan mengandung ID pengguna yang akan dibaca.
- async def read_user(id: str): - Ini adalah definisi fungsi read_user yang mengambil parameter id sebagai string.
- user = get_user(id) - Memanggil fungsi get_user untuk mengambil data pengguna berdasarkan ID yang diberikan. 

UPDATE

![ClassDiagram](./wsUpdate.png)

Penjelasan :
- @app.put("/update") - Ini adalah decorator yang menentukan metode HTTP yang digunakan (PUT) dan jalur endpoint (/update) untuk mencocokkan permintaan HTTP yang masuk.
- def update_user(user: UserUpdate): - Ini adalah definisi fungsi update_user yang mengambil objek user sebagai parameter. Objek user diketahui mengikuti struktur data UserUpdate.
- pst = conn.prepareStatement("UPDATE user SET username=?, password=?, nik=?, nohp=? WHERE id=?") - Membuat pernyataan SQL untuk memperbarui data pengguna dalam tabel user berdasarkan ID yang diberikan. Kolom-kolom yang diperbarui adalah username, password, nik, dan nohp.
- pst.setString(1, user.nama), pst.setString(2, user.password), pst.setString(3, user.nik), pst.setString(4, user.nohp), pst.setString(5, user.id) - Mengatur nilai-nilai parameter dalam pernyataan SQL sesuai dengan nilai-nilai yang ada dalam objek user.
- k = pst.executeUpdate() - Mengeksekusi perintah update dengan menggunakan pst.executeUpdate(). Variabel k akan berisi jumlah baris yang terpengaruh oleh perintah update.
- Jika k == 1, artinya satu baris data telah berhasil diperbarui. Maka, akan mengembalikan respon JSON dengan pesan "Data berhasil diupdate".

DELETE

![ClassDiagram](./wsDelete.png)

Penjelasan : 
- @app.delete("/user/{id}") - Ini adalah decorator yang menentukan metode HTTP yang digunakan (DELETE) dan jalur endpoint (/user/{id}) untuk mencocokkan permintaan HTTP yang masuk. {id} adalah parameter jalur yang akan mengandung ID pengguna yang akan dihapus.
- def delete_user_from_database(id: str): - Ini adalah definisi fungsi delete_user_from_database yang mengambil parameter id sebagai string.
- db = mysql.connector.connect(...) - Membuat koneksi ke database MySQL menggunakan modul mysql.connector.
- query = "DELETE FROM user WHERE id=%s" - Membuat pernyataan SQL untuk menghapus baris pengguna dari tabel user berdasarkan ID yang diberikan.
- values = (id,) - Membuat tupel values yang berisi nilai ID pengguna yang akan dihapus.
- cursor.execute(query, values) - Mengeksekusi pernyataan SQL dengan menggabungkan pernyataan dan nilai yang sesuai.
- cursor.close() dan db.close() - Menutup objek cursor dan koneksi database setelah selesai.

# 7 Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
![ClassDiagram](./gui.png)

![ClassDiagram](./no8.gif)
# 8 Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
![ClassDiagram](./JsonResult.png)
# 9 Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
[Klik disini](https://youtu.be/p58-L6-gCzE)
